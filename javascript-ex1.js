let arraysA = [[1, 2, 3], [1, 2, 2], [1, 1]];
let arraysB = [[1, 1], [2, 20]];

function calculator(array){
    var result = 0;
    for(i in array){
        for(j in array[i]){
            result += array[i][j];
        }
    }return result;
}

console.log(calculator(arraysA));
console.log(calculator(arraysB));